import Yencode from 'yencode';
import {ChildSocket} from './child_socket';
import {SocketErrorFromChild, MessageToChild, ResultFromChild, DownloadedSegment} from './model';
import {openSync, writeSync, closeSync, writeFileSync} from 'fs';
import {join} from 'path';
import {downloadPath} from './utils';

const socket = new ChildSocket(err => {
  if (process.send) {
    const msg: SocketErrorFromChild = {type: 'socket-error', err};
    process.send(msg);
  }
});

process.on('message', (message: MessageToChild) => {
  const {group, segmentId} = message;
  socket.whenConnected(() => {
    socket.auth(() =>
      socket.selectGroup(group, () => {
        downloadSegmentWithRetries(segmentId)
          .then(downloadedSegment => {
            // Saving buffer in file at specific position
            const fd = openSync(
              join(downloadPath(), message.title/*, downloadedSegment.fileName*/),
              'a'
            );
            writeSync(
              fd,
              downloadedSegment.buffer,
              undefined,
              undefined,
              downloadedSegment.beginOffset - 1
            );
            closeSync(fd);

            // Sending back results to master
            if (process.send) {
              const msg: ResultFromChild = {
                type: 'result',
                segmentId,
                info: {
                  fileIndex: message.fileIndex,
                  fileName: downloadedSegment.fileName,
                  beginOffset: downloadedSegment.beginOffset,
                  endOffset: downloadedSegment.endOffset,
                  totalSize: downloadedSegment.totalSize,
                },
              };
              process.send(msg);
            }
          })
          .catch(err => {
            if (process.send) {
              const msg: ResultFromChild = {
                type: 'result',
                segmentId,
                err: err.message,
              };
              process.send(msg);
            }
          });
      })
    );
  });
});

async function downloadSegmentWithRetries(segment: string): Promise<DownloadedSegment> {
  const MAX_TRY = 100;
  for (let i = 0; i < MAX_TRY; i++) {
    try {
      return await downloadSegment(segment);
    } catch (err) {
      console.log(`[CHILD - ${segment}] failed`, err.message);
    }
  }
  throw new Error(`Giving up after ${MAX_TRY} tries`);
}

function downloadSegment(articleId: string, checkCrc: boolean = true): Promise<DownloadedSegment> {
  return new Promise((resolve, reject) => {
    if (articleId[0] !== '<') {
      articleId = `<${articleId}>`;
    }

    // Timeout detection code
    const timeoutMs = 1000;
    let lastResponse = Date.now();
    let interval = setInterval(() => {
      if (Date.now() - lastResponse > timeoutMs) {
        clearInterval(interval);
        reject(new Error('Timeout'));
      }
    }, 1000);

    let articleStartParsed = false;
    let buffers: Buffer[] = [];

    let fileName: string = '';
    let beginOffset: string = '';
    let endOffset: string = '';
    let totalSize: string = '';

    function cleanup(): void {
      clearInterval(interval);
      buffers = [];
    }

    socket.sendCommand(
      `BODY ${articleId}`,
      [],
      (res: string, isDone) => {
        lastResponse = Date.now();

        // If the article headers have not been parsed yet, we are still in text
        // mode and look for them.
        let bufferStart: number | undefined;

        // Parsing header
        if (!articleStartParsed) {
          const beginIndex = res.indexOf('=ybegin', 0);
          if (beginIndex !== -1) {
            // End of header
            const headerEndStart = res.indexOf(' end=', beginIndex + 7) + 1;

            // Fetch file name
            const fileNameIndex = res.indexOf(' name=');
            const fileNameStart = fileNameIndex + ' name='.length;
            fileName = res.substr(
              fileNameStart,
              res.indexOf('\r\n', fileNameStart) - fileNameStart
            );

            // Fetch begin bytes offset
            const beginOffsetIndex = res.indexOf(' begin=');
            const beginOffsetStart = beginOffsetIndex + ' begin='.length;
            beginOffset = res.substr(
              beginOffsetStart,
              res.indexOf(' ', beginOffsetStart) - beginOffsetStart
            );

            // Fetch end bytes offset
            const endOffsetIndex = res.indexOf(' end=');
            const endOffsetStart = endOffsetIndex + ' end='.length;
            endOffset = res.substr(
              endOffsetStart,
              res.indexOf('\r\n', endOffsetStart) - endOffsetStart
            );

            // Fetch total size
            const totalSizeIndex = res.indexOf(' size=');
            const totalSizeStart = totalSizeIndex + ' size='.length;
            totalSize = res.substr(
              totalSizeStart,
              res.indexOf(' ', totalSizeStart) - totalSizeStart
            );

            // Start of encoded data
            bufferStart = res.indexOf('\n', headerEndStart) + 1;

            articleStartParsed = true;
          } else {
            cleanup();
            reject(new Error('First buffer has no =ybegin'));
            return;
          }
        }
        // If this is the last packet we received, we look for the end marker to
        // not decode that part.
        let bufferEnd: number | undefined;
        let expectedCRC: string | undefined;
        if (isDone) {
          const endIndex = res.lastIndexOf('=yend');
          if (endIndex !== -1) {
            bufferEnd = endIndex;
            expectedCRC = res
              .slice(bufferEnd)
              .split('crc32=')[1]
              .slice(0, 8)
              .toUpperCase();
          } else {
            // Check that previous buffer has =yend
            if (buffers.length > 0) {
              const newRes = Buffer.concat([
                buffers[buffers.length - 1],
                Buffer.from(res.slice(bufferStart || 0, bufferEnd || res.length), 'ascii'),
              ]).toString('ascii');
              const endIndex = newRes.lastIndexOf('=yend');
              if (endIndex !== -1) {
                bufferEnd = endIndex;
                expectedCRC = newRes
                  .slice(bufferEnd)
                  .split('crc32=')[1]
                  .slice(0, 8)
                  .toUpperCase();
              } else {
                cleanup();
                reject(new Error('Last buffer has no =yend'));
                return;
              }
            } else {
              cleanup();
              reject(new Error('Last buffer has no =yend'));
              return;
            }
          }
        }

        // Save the part that needs decoding
        buffers.push(Buffer.from(res.slice(bufferStart || 0, bufferEnd || res.length), 'ascii'));

        // When done, decode everything, do a CRC check and write to disk
        if (isDone) {
          const toDecode = Buffer.concat(buffers);
          const decoded = decodeBytes(toDecode);
          if (checkCrc) {
            const computedCRC = Yencode.crc32(decoded)
              .toString('hex')
              .toUpperCase();
            if (computedCRC !== expectedCRC) {
              cleanup();
              reject(new Error(`CRC failure (computed ${computedCRC}, expected ${expectedCRC}`));
              return;
            }
          }
          cleanup();
          resolve({
            buffer: toDecode,//decoded,
            fileName,
            beginOffset: parseInt(beginOffset, 10),
            endOffset: parseInt(endOffset, 10),
            totalSize: parseInt(totalSize, 10),
          });
        }
      },
      true
    );
  });
}

function decodeBytes(encoded: Buffer): Buffer {
  let escaped = false;
  let offset = 0;
  let previousWasCR = false;
  let previousWasCRLF = false;
  for (let i = 0; i < encoded.length; i++) {
    let c = encoded.readUInt8(i);
    // Skip newlines
    if (c === 13) {
      previousWasCR = true;
      previousWasCRLF = false;
      continue;
    }
    if (c === 10) {
      if (previousWasCR) {
        previousWasCRLF = true;
      } else {
        previousWasCR = false;
        previousWasCRLF = false;
      }
      continue;
    }
    if (previousWasCRLF && c === 46) {
      previousWasCR = false;
      previousWasCRLF = false;
      continue; // dot stuffing
    }

    previousWasCR = false;
    previousWasCRLF = false;

    // Look for escape chars
    if (c === 61 && !escaped) {
      escaped = true;
      continue;
    }

    // Special escape handling
    if (escaped) {
      c -= 64;
      if (c < 0) {
        c += 256
      }
      escaped = false;
    }

    if (c < 42 && c >= 0) {
      c += 214;
    } else {
      c -= 42;
    }
    offset = encoded.writeUInt8(c, offset);
  }
  return encoded.slice(0, offset);
}
