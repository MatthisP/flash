import {EventEmitter} from 'events';
import {NZBFile, SegmentInfo} from './model';

export class NZBStorage extends EventEmitter {
  private currentFileIndex = 0;
  private currentSegmentIndex = 0;

  // Map file id -> Map file segment id -> void (id defined, not downloaded)
  private segmentsNotDownloaded = new Map<string, Map<string, void>>();
  private segmentsToFileIndex: Map<string, number>;

  constructor(private files: NZBFile[]) {
    super();
    this.segmentsToFileIndex = new Map<string, number>();
    files.forEach((f, fileIndex) => {
      const segmentsNotDownloadedForFile = new Map<string, void>();
      this.segmentsNotDownloaded.set(f.internalId, segmentsNotDownloadedForFile);
      f.segments.forEach(s => {
        segmentsNotDownloadedForFile.set(s.id);
        this.segmentsToFileIndex.set(s.id, fileIndex);
      });
    });
  }

  public getNextSegment(): {fileIndex: number; group: string; segmentId: string} | undefined {
    const currentFile = this.files[this.currentFileIndex];
    if (currentFile === undefined) {
      if (this.currentSegmentIndex === 0) {
        this.currentFileIndex = 0;
        this.currentSegmentIndex = 1;
        return this.getNextSegment();
      }
      return undefined;
    }
    const currentSegment = currentFile.segments[this.currentSegmentIndex];
    if (currentSegment === undefined) {
      this.currentSegmentIndex = 1;
      this.currentFileIndex++;
      return this.getNextSegment();
    }
    const currentFileIndexToReturn = this.currentFileIndex;
    if (this.currentSegmentIndex === 0) {
      this.currentFileIndex++;
    } else {
      this.currentSegmentIndex++;
    }
    return {
      fileIndex: currentFileIndexToReturn,
      group: currentFile.group,
      segmentId: currentSegment.id,
    };
  }

  public saveSegmentData(segmentId: string, data: SegmentInfo): void {
    const fileIndex = this.segmentsToFileIndex.get(segmentId);
    if (fileIndex === undefined) {
      throw new Error(`untracked segmentId ${segmentId}`);
    }
    const file = this.files[fileIndex];
    if (fileIndex === undefined) {
      throw new Error(`untracked segmentId ${segmentId}`);
    }
    const segmentNotDownloadedForFile = this.segmentsNotDownloaded.get(file.internalId);
    if (segmentNotDownloadedForFile !== undefined) {
      segmentNotDownloadedForFile.delete(segmentId);
    }
    this.checkFile(file);
  }

  private checkFile(file: NZBFile): void {
    const notDownloadedSegments = this.segmentsNotDownloaded.get(file.internalId);
    const isCompleted = !notDownloadedSegments || notDownloadedSegments.size === 0;
    if (isCompleted) {
      this.emit('file-completed', file);
    }
  }
}
