import net from 'net';

type Res = string;

const options = {
  host: 'news.newshosting.com',
  user: 'matthis.perrin@gmail.com',
  pass: 'qn4rrevf4f',
  port: 119,
};

export class ChildSocket {
  private socket = new net.Socket();
  private socketConnected = false;
  private whenConnectedCallbacks: (() => void)[] = [];

  private isAuthenticated = false;
  private currentGroup: string | undefined = undefined;

  private commandCallback: ((res: Res, isDone: boolean) => void) | undefined;
  private expectedCodes: string[] = [];
  private multiLine: boolean = false;

  constructor(onSocketError: (err: string) => void) {
    this.socket.setEncoding('binary');
    this.socket.on('connect', () => this.handleConnect());
    this.socket.on('error', err => onSocketError(String(err)));
    this.socket.on('data', (data: string) => {
      const code = data.slice(0, 3);
      if (this.expectedCodes.length > 0 && this.expectedCodes.indexOf(code) === -1) {
        return;
      }
      const len = data.length;
      const isDone =
        !this.multiLine ||
        (data[len - 5] === '\r' &&
          data[len - 4] === '\n' &&
          data[len - 3] === '.' &&
          data[len - 2] === '\r' &&
          data[len - 1] === '\n');

      const cb = this.commandCallback;
      if (isDone && cb) {
        this.commandCallback = undefined;
        this.expectedCodes = [];
        this.multiLine = false;
        cb(data.slice(0, data.length - 5), true);
      } else if (cb) {
        cb(data, false);
      }
    });

    this.socket.connect(options.port, options.host);
  }

  public whenConnected(cb: () => void): void {
    if (this.socketConnected) {
      cb();
    } else {
      this.whenConnectedCallbacks.push(cb);
    }
  }

  public auth(cb: () => void): void {
    if (!this.isAuthenticated) {
      this.sendCommand(`AUTHINFO USER ${options.user}`, ['200', '381'], () => {
        this.sendCommand(`AUTHINFO PASS ${options.pass}`, ['281'], () => {
          this.isAuthenticated = true;
          cb();
        });
      });
    } else {
      cb();
    }
  }

  public selectGroup(groupId: string, cb: () => void): void {
    if (this.currentGroup !== groupId) {
      this.sendCommand(`GROUP ${groupId}`, ['211'], () => {
        this.currentGroup = groupId;
        cb();
      });
    } else {
      cb();
    }
  }

  public sendCommand(
    command: string,
    expectedCodes: string[],
    cb: (res: Res, isDone: boolean) => void,
    isMultiline: boolean = false
  ) {
    this.expectedCodes = expectedCodes;
    this.commandCallback = cb;
    this.multiLine = isMultiline;
    this.socket.write(`${command}\r\n`);
  }

  private handleConnect(): void {
    this.socketConnected = true;
    this.whenConnectedCallbacks.forEach(cb => cb());
    this.whenConnectedCallbacks = [];
  }
}
