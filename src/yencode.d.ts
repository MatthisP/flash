declare module 'yencode' {
  export function decode(toDecode: Buffer, dotStuffing: boolean): Buffer;
  export function crc32(toCheck: Buffer): Buffer;
}
