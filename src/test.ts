import path from 'path';
import fs from 'fs'
import { downloadNZB } from './main';

const NZB_FILE_PATH = path.join(__dirname, '..', 'test.nzb');
const nzbFile = fs.readFileSync(NZB_FILE_PATH).toString();
downloadNZB(nzbFile);