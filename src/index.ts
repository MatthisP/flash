import {NZBStorage} from './nzb_storage';
import {ChildProcessManager} from './child_process_manager';
import {NZBFile, ResultFromChild, Download, DownloadStatus, SegmentInfo} from './model';
import {Parser} from 'xml2js';

import * as http from 'http';
import {mkdirSync, readFileSync} from 'fs';
import {join} from 'path';
import {downloadPath} from './utils';
import {execSync} from 'child_process';

const downloads: {[id: string]: Download} = {};
const nzbs: {[id: string]: string} = {};

let isProcessIdle = true;

try {
  execSync(`mkdir -p "${downloadPath()}"`);
} catch {}

http
  .createServer((req, res) => {
    // Set CORS headers
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Request-Method', '*');
    res.setHeader('Access-Control-Allow-Methods', 'OPTIONS, GET, POST');
    res.setHeader('Access-Control-Allow-Headers', '*');
    // Handle preflight
    if (req.method === 'OPTIONS') {
      res.writeHead(200);
      res.end();
      return;
    }
    // Handle req
    if (req.method === 'GET') {
      res.write(JSON.stringify(downloads));
      res.end();
      return;
    }
    if (req.method === 'POST') {
      let buffer: Buffer[] = [];
      req
        .on('data', chunk => {
          buffer.push(chunk);
        })
        .on('end', () => {
          if (!isProcessIdle) {
            return;
          }
          const body = JSON.parse(Buffer.concat(buffer).toString());
          const d = downloads[body.id]
            ? downloads[body.id]
            : {
                id: body.id,
                title: body.title,
                status: DownloadStatus.CREATED,
                totalBytes: 0,
                downloadedBytes: 0,
              };
          switch (d.status) {
            case DownloadStatus.CREATED:
              d.status = DownloadStatus.FETCHING_NZB;
              console.log(`Received new download from extension ${JSON.stringify(d)}`);
              break;
            case DownloadStatus.FETCHING_NZB:
              if (body.nzb) {
                nzbs[body.id] = body.nzb;
                d.status = DownloadStatus.DOWNLOADING;
                console.log(`Received new NZB from extension ${JSON.stringify(d)}`);
                downloadNZB(body.id);
              }
              break;
          }
          downloads[body.id] = d;
          res.end();
        });
    }
  })
  .listen(80, '0.0.0.0', () => {
    console.log('Server listening on port 80');
  });

function downloadNZB(id: string): void {
  const title = downloads[id].title;
  const filesInfo: {[fileIndex: string]: SegmentInfo[]} = {};
  const dest = join(downloadPath(), title);
  // Creating download directory for this nzb
  try {
    mkdirSync(dest);
  } catch (_) {}
  // Empty destination folder
  try {
    execSync(`find "${dest}" -type f -delete`).toString();
  } catch (_) {}
  downloadNZB(nzbs[id])
}
