import {NZBStorage} from './nzb_storage';
import {ChildProcessManager} from './child_process_manager';
import {NZBFile, ResultFromChild, Download, DownloadStatus, SegmentInfo} from './model';
import {Parser} from 'xml2js';

let childProcessManager: ChildProcessManager | undefined;

export function downloadNZB(content: string): void {

  const parser = new Parser();
  parser.parseString(content, async (err: any, result: any) => {
    const files = (result.nzb.file as any[]).map(parseNZBFile);
    
    const storage = new NZBStorage(files);
    storage.on('file-completed', (file: NZBFile) => {
      console.log('Finished file with segments count:', file.segments.length);
    });
    if (childProcessManager !== undefined) {
      childProcessManager.destroy();
    }
    // isProcessIdle = false;
    childProcessManager = new ChildProcessManager(60);
    childProcessManager.on('idle-processes', () => {
      nextJob();
    });
    childProcessManager.on('job-completed', (msg: ResultFromChild) => {
      if (msg.err !== undefined) {
        console.error('Child error', msg.segmentId, msg.err);
        process.exit(1);
      } else if (msg.info !== undefined) {
        // // Saving file info
        // const fileInfo = filesInfo[msg.info.fileIndex] || [];
        // fileInfo.push(msg.info);
        // filesInfo[msg.info.fileIndex] = fileInfo;

        // // Saving API data
        // let finalTotalSize = 0;
        // let downloadedBytes = 0;
        // for (const fileIndex in filesInfo) {
        //   const segmentsInfo = filesInfo[fileIndex];
        //   let totalSize = 0;
        //   for (const segmentInfo of segmentsInfo) {
        //     totalSize = segmentInfo.totalSize;
        //     downloadedBytes += segmentInfo.endOffset - segmentInfo.beginOffset + 1;
        //   }
        //   finalTotalSize += totalSize;
        // }
        // downloads[id].totalBytes = finalTotalSize;
        // downloads[id].downloadedBytes = downloadedBytes;
        // if (
        //   files.length === Object.keys(filesInfo).length &&
        //   downloads[id].downloadedBytes > 0 &&
        //   downloads[id].downloadedBytes === downloads[id].totalBytes
        // ) {
        //   isProcessIdle = true;
        //   downloads[id].status = DownloadStatus.DOWNLOADED;
        // }

        // Saving storage data
        storage.saveSegmentData(msg.segmentId, msg.info);
      } else {
        console.error('Invalid child message', msg);
      }
    });
    nextJob();
    function nextJob(): void {
      const nextSegment = storage.getNextSegment();
      if (nextSegment === undefined) {
        return;
      }
      const {fileIndex, group, segmentId} = nextSegment;
      if (childProcessManager !== undefined) {
        const moreIdleProcesses = childProcessManager.scheduleJob({
          fileIndex,
          group,
          segmentId,
          title: `dummy-${Math.round(Math.random() * 1e10)}.txt`,
        });
        if (moreIdleProcesses) {
          nextJob();
        }
      }
    }
});
}

function parseNZBFile(data: any): NZBFile {
    const group = data.groups[0].group[0];
    const segments = data.segments[0].segment.map((v: any) => ({
      id: v._,
      size: parseFloat(v.$.bytes),
    }));
    return {
      internalId: String(Math.round(Math.random() * 1e12)),
      group,
      segments,
    };
  }
  