import {EventEmitter} from 'events';
import {fork, ChildProcess} from 'child_process';
import {MessageToChild, SocketErrorFromChild, ResultFromChild} from './model';

const CHILD_TIMEOUT_MS = 20000;

export class ChildProcessManager extends EventEmitter {
  private idleChildren: ChildProcess[] = [];
  private children: {process: ChildProcess; job: MessageToChild}[] = [];
  private jobs: MessageToChild[] = [];

  private processJobStart = new Map<ChildProcess, number>();
  private checkProcessTimeoutInterval: NodeJS.Timeout | undefined;

  constructor(childCount: number) {
    super();
    for (let i = 0; i < childCount; i++) {
      const childProcess = fork('./dist/child.js');
      this.idleChildren.push(childProcess);
      childProcess.on('message', (msg: ResultFromChild | SocketErrorFromChild) =>
        this.handleChildMessage(childProcess, msg)
      );
      this.checkProcessTimeoutInterval = setInterval(
        () => this.checkProcessTimeout(),
        CHILD_TIMEOUT_MS
      );
    }
  }

  // Return true if there are idle processes left
  public scheduleJob(job: MessageToChild): boolean {
    this.jobs.push(job);
    this.flushJobs();
    return this.idleChildren.length > 0;
  }

  public destroy(): void {
    for (let childProcess of this.idleChildren) {
      childProcess.kill();
    }
    for (let child of this.children) {
      child.process.kill();
    }
  }

  private checkProcessTimeout(): void {
    const now = Date.now();
    for (let [childProcess, startTime] of this.processJobStart.entries()) {
      if (now - startTime > CHILD_TIMEOUT_MS) {
        const child = this.children.filter(c => c.process === childProcess)[0];
        if (child !== undefined) {
          console.log(`Restarting process after timeout for job ${JSON.stringify(child.job)}`);
          this.restartProcess(child.process, child.job);
        }
      }
    }
  }

  private restartProcess(childProcess: ChildProcess, job: MessageToChild): void {
    this.processJobStart.delete(childProcess);
    this.children = this.children.filter(c => c.process !== childProcess);
    childProcess.kill();
    const newChildProcess = fork('./dist/child.js');
    process.on('message', msg => this.handleChildMessage(childProcess, msg));
    this.idleChildren.push(newChildProcess);
    this.scheduleJob(job);
  }

  private flushJobs(): void {
    while (this.jobs.length > 0 && this.idleChildren.length > 0) {
      const job = this.jobs.shift();
      const childProcess = this.idleChildren.shift();
      if (!job || !childProcess) {
        throw new Error(`Inconsistency! ${job} and ${childProcess}`);
      }
      this.children.push({process: childProcess, job});
      this.processJobStart.set(childProcess, Date.now());
      childProcess.send(job);
    }
  }

  private handleChildMessage(
    childProcess: ChildProcess,
    msg: ResultFromChild | SocketErrorFromChild
  ): void {
    this.processJobStart.delete(childProcess);
    const childIndex = this.children.map(c => c.process).indexOf(childProcess);
    if (childIndex === -1) {
      throw new Error(`Inconsistency! no child process registered for msg ${msg}`);
    }
    const [child] = this.children.splice(childIndex, 1);
    if (msg.type === 'socket-error') {
      console.log(`Restarting process after socket error for job ${JSON.stringify(child.job)}`);
      this.restartProcess(child.process, child.job);
    } else {
      this.idleChildren.push(childProcess);
      this.flushJobs();
      if (this.idleChildren.length > 0) {
        this.emit('idle-processes');
      }
      this.emit('job-completed', msg);
    }
  }
}
