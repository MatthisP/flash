export interface NZBFile {
  internalId: string;
  group: string;
  segments: {id: string}[];
}

export interface ResultFromChild {
  type: 'result';
  segmentId: string;
  info?: SegmentInfo;
  err?: string;
}

export interface SocketErrorFromChild {
  type: 'socket-error';
  err?: string;
}

export interface MessageToChild {
  fileIndex: number;
  group: string;
  segmentId: string;
  title: string;
}

export enum DownloadStatus {
  'CREATED',
  'FETCHING_NZB',
  'DOWNLOADING',
  'DOWNLOADED',
}

export interface Download {
  id: string;
  title: string;
  status: DownloadStatus;
  totalBytes: number;
  downloadedBytes: number;
}

export interface DownloadedSegment {
  buffer: Buffer;
  fileName: string;
  beginOffset: number;
  endOffset: number;
  totalSize: number;
}

export interface SegmentInfo {
  fileIndex: number;
  fileName: string;
  beginOffset: number;
  endOffset: number;
  totalSize: number;
}
