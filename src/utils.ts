import path from 'path';

export function downloadPath(): string {
  return path.join(__dirname, '..', 'data')
  // return process.env.DOWNLOAD_PATH || '/data';
}
