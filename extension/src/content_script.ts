import * as $ from 'jquery';
import {fetchFileCount} from './file_counts';
import {UIElement, Download, DownloadStatus} from './model';

const serverUrl = 'https://129.168.0.41:1612/';
// const serverUrl = 'http://127.0.0.1/';
const iconUrl = chrome.extension.getURL('icon.png');

const uiElements: {[id: string]: UIElement} = {};
const downloads: {[id: string]: Download} = {};
const speeds: {[id: string]: number} = {};

$('#browsetable .row').each((index, element) => {
  const elem = $(element);

  const id = elem.attr('id');
  if (id === undefined) {
    return;
  }

  const title = elem.find('.title').text();
  if (title === undefined) {
    return;
  }

  const links = elem.find('.icons .hidden-sm.hidden-xs');

  const nzbUrl = links.find('a:eq(0)').attr('href');
  if (nzbUrl === undefined) {
    return;
  }

  const downloadBar = $(
    '<div style="height:20px;width:100%;margin-top:4px;display:none;position:relative"><div class="download-progress" style="background-color:green;height:100%;position:absolute"></div><div class="download-text" style="text-align:center;color:white;position:absolute;width:100%"></div></div>'
  );
  const iconHtml = $(
    `<img style="width:20px;position:absolute;left:56px;cursor:pointer;" src="${iconUrl}" />`
  );
  const infoButtons = elem.find('.btns');
  const fileCountButton = $(
    '<a class="btn btn-xs label rndbtn"data-html="true" data-toggle="tooltip" title="" data-original-title="">loading...</a>'
  );
  infoButtons.append(fileCountButton);

  iconHtml.on('click', () => {
    $.ajax({method: 'POST', url: serverUrl, data: JSON.stringify({id, title})})
      .done(() => {
        $.ajax({
          method: 'GET',
          url: nzbUrl,
        })
          .done(nzb => {
            $.ajax({method: 'POST', url: serverUrl, data: JSON.stringify({id, nzb})}).fail(fail);
          })
          .fail(fail);
      })
      .fail(fail);
  });

  links.append(iconHtml);
  elem.find('.item').append(downloadBar);

  const uiElement = {id, nzbUrl, downloadBar, iconHtml, fileCountButton};
  uiElements[id] = uiElement;

  // fetchFileCount(uiElement);
});

let lastTick = Date.now();

// chrome.runtime.sendMessage(
//   {
//     method: 'GET',
//     action: 'xhttp',
//     url: serverUrl,
//   },
//   function(responseText) {
//     alert(responseText);
//     /*Callback function to deal with the response*/
//   }
// );

function fetchDownloads() {
  $.ajax({
    method: 'GET',
    url: serverUrl,
  })
    .done(data => {
      const now = Date.now();
      const newDownloads = JSON.parse(data) as {[id: string]: Download};
      for (const id in newDownloads) {
        const d = newDownloads[id];
        const oldD = downloads[id];
        if (d.status === DownloadStatus.DOWNLOADING) {
          speeds[id] = (1000 * (d.downloadedBytes - oldD.downloadedBytes)) / (now - lastTick);
        } else {
          speeds[id] = undefined;
        }
        downloads[id] = d;
      }
      lastTick = now;
      refreshUI();
    })
    .fail(fail);
}

setInterval(fetchDownloads, 500);

function fail() {
  console.log('I am erroring like a boss!');
}

function refreshUI() {
  for (const id in downloads) {
    const d = downloads[id];
    const e = uiElements[id];
    switch (d.status) {
      case DownloadStatus.CREATED:
      case DownloadStatus.FETCHING_NZB:
        e.downloadBar.css({
          display: 'block',
          backgroundColor: 'blue',
        });
        e.downloadBar.find('.download-text').text('Fetching NZB...');
        break;
      case DownloadStatus.DOWNLOADING:
        e.downloadBar.css({
          display: 'block',
          backgroundColor: 'gray',
        });
        const percent = d.totalBytes > 0 ? Math.round((100 * d.downloadedBytes) / d.totalBytes) : 0;
        const speed = speeds[id] ? Math.round(speeds[id] / (1024 * 1024)) : 0;
        e.downloadBar.find('.download-text').text(`${percent}% @ ${speed} MB/s`);
        e.downloadBar.find('.download-progress').css({width: `${percent}%`});
        break;
      case DownloadStatus.DOWNLOADED:
        e.downloadBar.css({
          display: 'block',
          backgroundColor: 'green',
        });
        e.downloadBar.find('.download-text').text('Downloaded');
        break;
      default:
        e.downloadBar.css({
          display: 'none',
          backgroundColor: null,
        });
        e.downloadBar.find('.download-text').text('');
        break;
    }
  }
}
