export enum DownloadStatus {
  'CREATED',
  'FETCHING_NZB',
  'DOWNLOADING',
  'DOWNLOADED',
}

export interface Download {
  id: string;
  status: DownloadStatus;
  totalBytes: number;
  downloadedBytes: number;
}

export interface UIElement {
  id: string;
  nzbUrl: string;
  downloadBar: JQuery<HTMLElement>;
  iconHtml: JQuery<HTMLElement>;
  fileCountButton: JQuery<HTMLElement>;
}
