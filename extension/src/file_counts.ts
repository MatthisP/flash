import * as $ from 'jquery';
import {UIElement} from './model';

function rateLimit<Res, T extends (...args: any[]) => Promise<Res>>(func: T, limit: number): T {
  let scheduled: {func: T; context: any; args: any[]; resolve: (res: Res) => void}[] = [];
  let lastExecution = 0;
  let isExecutingScheduled = false;

  function execNextScheduled(): void {
    lastExecution = Date.now();
    isExecutingScheduled = true;
    const nextScheduled = scheduled.shift();
    if (nextScheduled !== undefined) {
      const {func, context, args, resolve} = nextScheduled;
      func
        .apply(context, args)
        .then(res => {
          resolve(res);
          setTimeout(execNextScheduled, limit - (Date.now() - lastExecution));
        })
        .catch(() => {
          scheduled.unshift(nextScheduled);
          setTimeout(execNextScheduled, 500);
        });
    } else {
      isExecutingScheduled = false;
    }
  }

  return function(...args: any[]) {
    return new Promise<Res>(resolve => {
      const context = this;
      scheduled.push({func, context, args, resolve});
      if (!isExecutingScheduled) {
        if (Date.now() - lastExecution > limit) {
          execNextScheduled();
        } else {
          setTimeout(() => {
            if (!isExecutingScheduled) {
              execNextScheduled();
            }
          }, limit - (Date.now() - lastExecution));
        }
      }
    });
  } as T;
}

const multiplicator = {
  KB: 1024,
  MB: 1024 * 1024,
  GB: 1024 * 1024 * 1024,
};

export const fetchFileCount = rateLimit((uiElement: UIElement): Promise<void> => {
  return new Promise<void>((resolve, reject) => {
    const fileListUrl = `https://nzb.su/filelist/${uiElement.id.slice(4)}`;
    $.ajax({
      method: 'GET',
      url: fileListUrl,
    })
      .done(data => {
        const lines = $(data).find('.table tr');
        let fileCount = 0;
        let largestFile = 0;
        let largestFileStr = '';
        let largestFileName = '';
        lines.each((i, e) => {
          const cells = $(e).find('td');
          if (cells.length > 2) {
            fileCount++;
            const sizeStr = cells.get(cells.length - 1).innerHTML;
            const nameStr = cells.get(1).innerHTML;
            const sizeMatch = sizeStr.match(/([0-9\.]+).*?([KMGT]?B)/);
            if (sizeMatch.length === 3) {
              const sizeValue = parseFloat(sizeMatch[1]);
              const sizeUnit = sizeMatch[2];
              const sizeMultiplicator = multiplicator[sizeUnit] || 1;
              if (!isNaN(sizeValue)) {
                const size = sizeValue * sizeMultiplicator;
                if (size > largestFile) {
                  largestFile = size;
                  largestFileStr = `${sizeValue} ${sizeUnit}`;
                  largestFileName = nameStr;
                }
              }
            }
          }
        });
        const tooltip = `'Largest file name: ${largestFileName}`;
        const contentFileCount = `${fileCount} file${fileCount > 1 ? 's' : ''}`;
        const contentFileSize = `${fileCount > 1 ? `largest: ${largestFileStr}` : largestFileStr}`;
        uiElement.fileCountButton
          .text(`${contentFileCount} (${contentFileSize})`)
          .attr('data-original-title', tooltip);
        if (fileCount < 10) {
          uiElement.fileCountButton.css('color', 'rgb(255, 80, 80)');
        }
        resolve();
      })
      .fail(() => reject());
  });
}, 200);
